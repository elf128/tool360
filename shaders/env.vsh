
// Vertex shader

const vec4 vtxPos[4] = vec4[4] (
     vec4( -1.0,  1.0, 1.0, 1.0 ),
     vec4( -1.0, -1.0, 1.0, 1.0 ),
     vec4(  1.0,  1.0, 1.0, 1.0 ),
     vec4(  1.0, -1.0, 1.0, 1.0 )
);

out     vec3 v2f_Ray;
out     vec3 v2f_View;
uniform vec2 p_fov;
uniform mat3 p_view;
uniform lowp vec3 p_lens;

const float toRad = 360.0f / 3.1415926f;

void main()
{
    vec4 pos    = vtxPos[ gl_VertexID ];
    
    gl_Position = pos;
    vec3 fov    = vec3( p_fov, 1.0 );
    fov.x       = tan( fov.x / toRad ) / ( p_lens.z + 1.0 );
    fov.y       = tan( fov.y / toRad ) / ( p_lens.z + 1.0 );
    vec3 view   = pos.xzy * fov.xzy;
    
    v2f_View    = vec3( 0.0, 1.0, 0.0 ) * p_view;
    v2f_Ray     = view * p_view;
}
