
#include "common.fh"

uniform samplerCube env;

layout(location = 0) out lowp vec4 color;

void main()
{
    lowp vec3 dist = getMainRay();
    
    lowp vec4 texColor = texture( env, dist );
    //lowp vec4 texColor = vec4( normalize( v2f_View ), 2.0f );
    
    //texColor *= 0.5;
    //texColor += 0.5;
    
    color = vec4( texColor.rgb, 1.0f );
}
