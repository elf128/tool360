
#define COMPUTEPATCHSIZE 32
#define IMGFMT rgba8 

layout (local_size_x = COMPUTEPATCHSIZE, local_size_y = COMPUTEPATCHSIZE) in;

layout(binding=0, IMGFMT) uniform readonly  lowp image2D   inputImage; // Use a sampler to improve performance  
layout(binding=1, IMGFMT) uniform writeonly lowp imageCube resultImage;

uniform int radius;

const lowp mat3 cubeDef[6] = mat3[6](
        mat3( vec3( 0.0, 0.0, 1.0 ),
              vec3( 0.0,-1.0, 1.0 ),
              vec3(-1.0, 0.0, 1.0 ) ),
              
        mat3( vec3( 0.0, 0.0, 0.0 ),
              vec3( 0.0,-1.0, 1.0 ),
              vec3( 1.0, 0.0, 0.0 ) ),
              
        mat3( vec3( 1.0, 0.0, 0.0 ),
              vec3( 0.0, 0.0, 1.0 ),
              vec3( 0.0, 1.0, 0.0 ) ),
              
        mat3( vec3( 1.0, 0.0, 0.0 ),
              vec3( 0.0, 0.0, 0.0 ),
              vec3( 0.0,-1.0, 1.0 ) ),
              
        mat3( vec3( 1.0, 0.0, 0.0 ),
              vec3( 0.0,-1.0, 1.0 ),
              vec3( 0.0, 0.0, 1.0 ) ),
              
        mat3( vec3(-1.0, 0.0, 1.0 ),
              vec3( 0.0,-1.0, 1.0 ),
              vec3( 0.0, 0.0, 0.0 ) )
    );


lowp vec3 getCubePixelDir( ivec2 pos, ivec2 size, int side )
{
    return vec3( float( pos.x )/float( size.x-1 ),
                 float( pos.y )/float( size.y-1 ), 
                 1.0 ) 
                * cubeDef[ side ];
}

void main()
{
    vec4 res = vec4(0.0);

    ivec2 imgSize = imageSize(resultImage);
    ivec3 pos  = ivec3( int( gl_GlobalInvocationID.x ),
                        int( gl_GlobalInvocationID.y ),
                        int( gl_GlobalInvocationID.z ) );
    
    res.rgb = getCubePixelDir( pos.xy, imgSize, pos.z );
    
    imageStore( resultImage, pos, res );
};
