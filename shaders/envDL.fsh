
#include "common.fh"

in      lowp vec3 v2f_View;
in      lowp vec3 v2f_Ray;
uniform sampler2D env;
uniform lowp vec3 p_lens;

layout(location = 0) out lowp vec4 color;

void main()
{
    lowp vec3 ray  = normalize( v2f_Ray );
    lowp vec3 view = normalize( v2f_View );
    lowp vec3 dist = SGP( ray, view, p_lens.z );
    lowp vec4 texColor = texture( env, View2ER( dist ) );
    //lowp vec4 texColor = vec4( normalize( v2f_View ), 2.0f );
    
    //texColor *= 0.5;
    //texColor += 0.5;
    
    color = vec4( texColor.rgb, 1.0f );
}
