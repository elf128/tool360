/*
 * Input.cpp
 *
 *  Created on: Feb 24, 2020
 *      Author: vlad
 */

#include "Input.h"

Input::Input()
    : Texture ( nullptr )
{

}

Input::~Input() {}

void Input::makeEmpty( QSize size )
{
    if( Texture )
        delete Texture;

    Texture = new QOpenGLTexture( QOpenGLTexture::Target2D );

    Texture->create();
    Texture->setSize( size.width(), size.height(), 1 );
    Texture->setWrapMode( QOpenGLTexture::ClampToEdge );
    Texture->setMinificationFilter( QOpenGLTexture::Linear );
    Texture->setMagnificationFilter(QOpenGLTexture::Linear );
    //Texture->setFormat()
    //Texture->allocateStorage();
    Texture->allocateStorage( QOpenGLTexture::PixelFormat::RGBA_Integer, QOpenGLTexture::PixelType::UInt32_RGBA8 );

}

void Input::makeFromFile( QString filename )
{
    if( Texture )
        delete Texture;

    Texture = new QOpenGLTexture( QImage(filename).mirrored() );

    Texture->setMinificationFilter(  QOpenGLTexture::Nearest);
    Texture->setMagnificationFilter( QOpenGLTexture::Linear );

    Texture->setWrapMode(QOpenGLTexture::ClampToEdge );
}
