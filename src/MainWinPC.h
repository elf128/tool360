/*
 * MainWinPC.h
 *
 *  Created on: Feb 28, 2020
 *      Author: vlad
 */

#ifndef MAINWINPC_H_
#define MAINWINPC_H_

#include <QMainWindow>

class Toolbar;
class ViewParams;
class Viewport;
class VoidProperty;

QT_FORWARD_DECLARE_CLASS(QMenu)

class MainWinPC : public QMainWindow
{
    Q_OBJECT

public:
    typedef QMap<QString, QSize> CustomSizeHintMap;

    explicit MainWinPC( QWidget *parent = Q_NULLPTR, Qt::WindowFlags flags = 0 );

public slots:
    //void actionTriggered(QAction *action);
    //void saveLayout();
    //void loadLayout();
    //void switchLayoutDirection();
    //void setDockOptions();

    //void createDockWidget();
    //void destroyDockWidget(QAction *action);

    void update();
private:
    void makeToolBar();
    void makeDockWidgets();
    void setupProperties();

    Toolbar*    toolbar;
    ViewParams* params;
    QTimer*     timer;
    Viewport*   viewport;

    QList< VoidProperty* > properties;
    //void setupMenuBar();

    //QList<ToolBar*> toolBars;
    //QMenu *dockWidgetMenu;
    //QMenu *mainWindowMenu;
    //QList<QDockWidget *> extraDockWidgets;
    //QMenu *destroyDockWidgetMenu;
};

#endif /* MAINWINPC_H_ */
