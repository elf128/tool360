/*
 * GPU.h
 *
 *  Created on: Mar 5, 2020
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef GPUIMPL_H_
#define GPUIMPL_H_

#include <QOpenGLWindow>
#include <QOpenGLTexture>
#include "Input.h"

#include "primitives.h"
#include "trackball.h"

class ViewData
{
public:

    static QVector2D g_FOV; // degrees
    static QVector3D g_Lens;
    static float     g_FPS;
    static float     g_FrameTime;
    static int       g_width;
    static int       g_height;
    static QVector< QOpenGLTexture* >   g_textures;
    static Input                        g_input;
    static QFileSystemWatcher*          g_watcher;

    static TrackBall g_trackBall;

    static void   setHFOV( const float p_hFOV );

    static inline float     getHFOV()      { return g_FOV.x(); }
    static inline float     getVFOV()      { return g_FOV.y(); }
    static inline float     getFPS()       { return g_FPS; }
    static inline float     getFrameTime() { return g_FrameTime; }

    static void rebuildFOV();
    static inline QVector2D getBallSpace( const QPoint  pos ) { return getBallSpace( QPointF( pos ) ); }
    static inline QVector2D getBallSpace( const QPointF pos )
    {
        float M = sqrtf( float( g_width ) * g_width + float( g_height ) * g_height ) ;
        return QVector2D( ( pos.x() - g_width * 0.5f  ) * 2.0f / float( M ),
                          ( g_height * 0.5f - pos.y() ) * 2.0f / float( M )  );
    }

    static void __init__();
    static void __destroy__();
    static void drawCube( QOpenGLTexture* src );
    static void drawER(   QOpenGLTexture* src );
    static void computeCube( QOpenGLTexture* src, QOpenGLTexture* tgt );
    static void reloadShaders();

private:

    static int      mainTextureSize;
    static QString  profile;

    static QMutex                           glLock;
    static QOpenGLShaderProgram*            ShowERProgram;
    static QOpenGLShaderProgram*            ShowCubeProgram;
    static QOpenGLShaderProgram*            ComputeCubeProgram;

    static Trivial< 4, GL_TRIANGLE_STRIP >  background;

    friend QString buildShaderSourceFromFile( const QFileInfo filename, const int incLevel );
};


#endif /* GPUIMPL_H_ */
