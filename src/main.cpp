/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//#include <QGuiApplication>
#include <QApplication>
#include <QSurfaceFormat>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <QDebug>
#include <QPair>

#include "Viewport.h"
#if defined(__ANDROID_API__)
#define mainClass Viewport
#else
#include "MainWinPC.h"
#define mainClass MainWinPC
#endif

bool OGLSupports( int major, int minor, bool gles = false, QSurfaceFormat::OpenGLContextProfile profile = QSurfaceFormat::NoProfile)
{
    QOpenGLContext ctx;
    QSurfaceFormat fmt;
    fmt.setVersion( major, minor );

    if (gles)
        fmt.setRenderableType(QSurfaceFormat::OpenGLES);
    else
    {
        fmt.setRenderableType(QSurfaceFormat::OpenGL);
        fmt.setProfile(profile);
    }

    ctx.setFormat(fmt);
    ctx.create();

    if (!ctx.isValid())
        return false;

    int ctxMajor = ctx.format().majorVersion();
    int ctxMinor = ctx.format().minorVersion();
    bool isGles = (ctx.format().renderableType() == QSurfaceFormat::OpenGLES);

    if (isGles != gles) return false;
    if (ctxMajor < major) return false;

    if (ctxMajor == major && ctxMinor < minor)
        return false;

    if (!gles && ctx.format().profile() != profile)
        return false;

    return true;
}

void printSupport()
{
    const int version[] = {
            0x0200,
            0x0201,
            0x0300,
            0x0301,
            0x0302,
            0x0303,
            0x0400,
            0x0401,
            0x0402,
            0x0403,
            0x0404,
            0x0405
    };

    qDebug() << "Available support for GL/GLES:";
    qDebug() << "      noprof,  core,    compat,  GLES";

    for( int i = 0; i < int( sizeof( version )/sizeof(int) ); i++ )
    {
        const int ver = version[ i ];
        const int M = ver >> 8;
        const int m = ver & 0xFF;

        bool noprof = OGLSupports( M, m, false );
        bool core   = OGLSupports( M, m, false, QSurfaceFormat::CoreProfile );
        bool compat = OGLSupports( M, m, false, QSurfaceFormat::CompatibilityProfile );
        bool gles   = OGLSupports( M, m, true );

        qDebug() << M << m
                 << ( noprof ? "   yes  " : "    no  " )
                 << ( core   ? "   yes  " : "    no  " )
                 << ( compat ? "   yes  " : "    no  " )
                 << ( gles   ? "   yes  " : "    no  " );
    }

    qDebug() << "----------------------------------------";
}

int main( int argc, char **argv )
{
    QApplication app(argc, argv);
    //QGuiApplication app(argc, argv);

#if !defined(QT_NO_DEBUG)
    printSupport();
#endif

    int maxTextureSize = 4096;

    QSurfaceFormat fmt;

    fmt.setDepthBufferSize( 24 );

    // Request OpenGL ES 3.1 context, as this is a GLES example.
    // If not available, go for OpenGL 4.3 core.
    if ( OGLSupports( 3, 0, true ) )
    {
        qDebug("Requesting 3.1 GLES context");
        fmt.setVersion( 3, 0 );
        fmt.setRenderableType( QSurfaceFormat::OpenGLES );
    }
    else if ( OGLSupports( 4, 3, false, QSurfaceFormat::CoreProfile ) )
    {
        qDebug("Requesting 4.3 core context");
        fmt.setVersion(4, 3);
        fmt.setRenderableType( QSurfaceFormat::OpenGL );
        fmt.setProfile( QSurfaceFormat::CoreProfile );
    }
    else
    {
        qWarning("Error: This system does not support OpenGL Compute Shaders! Exiting.");
        return -1;
    }

    QSurfaceFormat::setDefaultFormat(fmt);

    mainClass mainView;
    mainView.show();

    return app.exec();
}

