/*
 * Input.h
 *
 *  Created on: Feb 24, 2020
 *      Author: vlad
 */

#ifndef INPUT_H_
#define INPUT_H_

#include <QOpenGLTexture>

enum class DataType
{
    Equirect,
    DualLens,
};

class Input
{

    QOpenGLTexture* Texture;

public:
    Input();
    virtual ~Input();

    inline QOpenGLTexture* getTexture() const { return Texture; }

    void makeEmpty( QSize size );
    void makeFromFile( QString filename );

};

#endif /* INPUT_H_ */
