
#include <QDebug>
#include <QtGui/qmatrix4x4.h>
#include <QtGui/qvector3d.h>
#include <math.h>
#include "Viewport.h"

#include "gpuImpl.h"

//============================================================================//
//                                    Scene                                   //
//============================================================================//

Viewport::Viewport( CLASS_PARAM *parent )
#if defined(__ANDROID_API__)
    : CLASS_PARENT( NoPartialUpdate, parent )
#else
    : CLASS_PARENT( parent )
#endif
    , m_frame             ( 0 )
    , m_ms                ( 0 )
    , m_lastTime          ( 0 )

{
    m_timer = new QTimer( this );
    m_timer->setInterval( 16 );
    connect( m_timer, SIGNAL( timeout() ), this, SLOT( update() ) );
    m_timer->start();

    m_time.start();

}

Viewport::~Viewport()
{
    makeCurrent();

    m_timer->stop();
    delete m_timer;

    ViewData::__destroy__();

    qDebug() << "-----------";
    qDebug() << "Viewport has been destroyed ";
}

void Viewport::initializeGL()
{
    ViewData::g_watcher = &m_watcher;

    ViewData::__init__();
    connect( &m_watcher, SIGNAL( fileChanged( QString ) ), this, SLOT( reloadProxy() ) );
}

void Viewport::resizeGL(int w, int h)
{
    ViewData::g_width  = w;
    ViewData::g_height = h;
    ViewData::rebuildFOV();
}

void Viewport::paintGL()
{
    ViewData::computeCube( ViewData::g_input.getTexture(), ViewData::g_textures[1] );
    ViewData::drawCube(    ViewData::g_textures[1] );

    ++m_frame;
    int ms =  m_time.elapsed();
    m_ms += ms - m_lastTime;
    m_lastTime = ms;

    ViewData::g_FrameTime = float( m_ms ) / m_frame;
    ViewData::g_FPS       = 1000.0f / ViewData::g_FrameTime;
    if ( m_frame > 16 )
    {
        m_frame--;
        m_ms -= int( ViewData::g_FrameTime );
    }
}

void Viewport::reloadProxy()
{
    ViewData::reloadShaders();
}

#if defined(__ANDROID_API__)

void Viewport::touchEvent(QTouchEvent *event)
{
    static int touchID = -1;

//    QWindow::touchEvent(event);
//    if (event->isAccepted())
//        return;

    const int touchCount = event->touchPoints().size();

    if (touchCount == 0)
    {
        if ( ViewData::g_trackBall.isPressed() )
            ViewData::g_trackBall.release();

        touchID = -1;
        event->accept();
        return;
        //qDebug() << "release";
    }

    if ( ViewData::g_trackBall.isPressed() )
    {
        bool used = false;
        for( auto point = event->touchPoints().begin(); point != event->touchPoints().end(); ++point )
        {
            if ( point->id() == touchID )
            {
                if ( ( point->state() & Qt::TouchPointReleased ) == 0 )
                {
                    ViewData::g_trackBall.move( ViewData::getBallSpace( point->pos() ));
                    used = true;
                    event->accept();
                    //qDebug() << "move";
                    return;
                }
            }
        }
        ViewData::g_trackBall.release();
        touchID = -1;
        //qDebug() << "id is lost";
    }
    else
    {
        touchID = event->touchPoints()[0].id();
        ViewData::g_trackBall.push( ViewData::getBallSpace( event->touchPoints()[0].pos() ));
        //qDebug() << "new touch";
    }

    event->accept();
}

#else //not __ANDROID_API__

void Viewport::mouseMoveEvent(QMouseEvent *event)
{

    QWidget::mouseMoveEvent(event);
    if (event->isAccepted())
        return;

    if (event->buttons() & Qt::LeftButton)
    {
        ViewData::g_trackBall.move( ViewData::getBallSpace(event->pos()) );
        event->accept();
    }
    else
        ViewData::g_trackBall.release();

}

void Viewport::mousePressEvent( QMouseEvent *event )
{
    QWidget::mousePressEvent(event);
    if ( event->isAccepted() )
        return;

    if (event->buttons() & Qt::LeftButton)
    {
        ViewData::g_trackBall.push( ViewData::getBallSpace(event->pos()) );
        event->accept();
    }
}

void Viewport::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);
    if (event->isAccepted())
        return;

    if (event->button() == Qt::LeftButton)
    {
        ViewData::g_trackBall.release();
        event->accept();
    }
}

void Viewport::wheelEvent(QWheelEvent * event)
{
    QWidget::wheelEvent(event);

    if (event->isAccepted())
        return;

    float fov = ViewData::g_FOV.x() - event->delta()* 0.1f;
    qDebug() << "Old FOV" << ViewData::g_FOV.x() << "New FOV" << fov;

    ViewData::g_FOV.setX( (fov > 10.0f )?( (fov<170.0f)?fov:170.0f ):10.0f );

    event->accept();

    ViewData::rebuildFOV();
}

#endif
