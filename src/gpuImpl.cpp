/*
 * GPU.cpp
 *
 *  Created on: Mar 5, 2020
 *      Author: Vlad A. <elf128@gmail.com>
 */

#include "gpuImpl.h"
#include "Input.h"
#include <qopenglext.h>

const int maxTextureSize   = 1024;
const int computeGoupeSize = 32;

#if defined(__ANDROID_API__)
#define SHADER_ROOT ":/shaders/"
#else
#define SHADER_ROOT "./shaders/"
#endif

QString buildShaderSourceFromFile( const QFileInfo filename, const QString header, const int incLevel )
{
    QFile source( filename.absoluteFilePath() );
    QString text;

    if ( incLevel == 0 )
        text = QString("#version ") + ViewData::profile + "\n";

    text.append( header );

    if ( source.open( QIODevice::ReadOnly ) )
    {
        while (1)
        {
            QString line = QString::fromUtf8( source.readLine( 1024 ) );
            if (line == "" )
                break;
            if ( line.startsWith("#include ", Qt::CaseSensitive ) )
            {
                QString name = line.right( line.size() - 9 );
                name = name.right( name.size() - name.indexOf('"')     - 1 );
                name = name.left( name.lastIndexOf('"') );

                text.append(
                        buildShaderSourceFromFile(
                                QFileInfo( filename.absoluteDir(), name ),
                                "",
                                incLevel + 1
                                )
                        );
            }
            else
                text.append( line );
        }
        bool watching = ViewData::g_watcher->addPath( filename.absoluteFilePath() );
        qDebug() << "Watching shader: " << filename.absoluteFilePath() << ( (watching)? "Success": "Failure" );

    }
    else
        qWarning() << "Failed to open " << filename.absoluteFilePath() ;

    return text;
}

void ViewData::__init__()
{
    g_FOV       = QVector2D( 120.0f, 120.0f );
    g_Lens      = QVector3D( 0.0f, 0.0f, 0.0f );
    g_FPS       = 0.0f;
    g_FrameTime = 100.0f;
    g_width     = 32;
    g_height    = 32;
    g_trackBall = TrackBall( 0.005f, QVector3D(0, 0, 1), TrackBall::TrackMode::Sphere );

    ShowERProgram      = nullptr;
    ShowCubeProgram    = nullptr;
    ComputeCubeProgram = nullptr;

    mainTextureSize = maxTextureSize;

    QOpenGLContext *ctx = QOpenGLContext::currentContext();

    profile = QString("%1%2 %3")
            .arg( ctx->format().majorVersion() )
            .arg( ctx->format().minorVersion()*10 )
            .arg( ( ( ctx->format().renderableType() == QSurfaceFormat::OpenGLES ) ? ("es") : ("core")) );

    qDebug() << "Got a "
             << ctx->format().majorVersion()
             << ctx->format().minorVersion()
             << ( ( ctx->format().renderableType() == QSurfaceFormat::OpenGLES ) ? (" GLES") : (" GL"))
             << " context. version str " << profile;

    background.__init__();

    //g_input.makeFromFile( ":/equirect/SAM_104_0965.jpg");
    g_input.makeFromFile( "./duallens/360_1017.JPG");

    //QStringList filter;
    //QList<QFileInfo> files;

    // Load all .png files as textures
    //filter = QStringList( "*.png" );
    //files  = QDir( ":/textures/" ).entryInfoList( filter, QDir::Files | QDir::Readable );


    QOpenGLTexture *texture = new QOpenGLTexture( QOpenGLTexture::Target2D );
    texture->setFormat( g_input.getTexture()->format() );
    texture->setSize( 512, 512 );
    texture->allocateStorage(QOpenGLTexture::RGBA,QOpenGLTexture::UInt8);

    ViewData::g_textures << texture;

    texture = new QOpenGLTexture( QOpenGLTexture::TargetCubeMap );
    texture->setFormat( g_input.getTexture()->format() );
    texture->setSize( 512, 512 );
    texture->allocateStorage(QOpenGLTexture::RGBA,QOpenGLTexture::UInt8);

    ViewData::g_textures << texture;

    qDebug() << "textures: " << ViewData::g_textures.count();

    reloadShaders();

}

void ViewData::__destroy__()
{
    foreach( QOpenGLTexture *texture, ViewData::g_textures ) if ( texture )
        delete texture;

    if ( ShowERProgram )       delete ShowERProgram;
    if ( ShowCubeProgram )     delete ShowCubeProgram;
    if ( ComputeCubeProgram )  delete ComputeCubeProgram;

    background.__destroy__();

    if ( g_input.getTexture() ) delete g_input.getTexture();

}

void ViewData::reloadShaders()
{
    qDebug() << "Shader reload";

    const QFileInfo vEnvName    (SHADER_ROOT"env.vsh");
    const QFileInfo fEnvERName  (SHADER_ROOT"/envER.fsh");
    const QFileInfo fEnvCubeName(SHADER_ROOT"/envCUBE.fsh");
    const QFileInfo ComputeName (SHADER_ROOT"/Compute.csh");

    if (ShowERProgram)
        delete ShowERProgram;

    ShowERProgram = new QOpenGLShaderProgram;
    ShowERProgram->addShaderFromSourceCode(
            QOpenGLShader::Vertex,
            buildShaderSourceFromFile( vEnvName, 0 ));
    ShowERProgram->addShaderFromSourceCode(
            QOpenGLShader::Fragment,
            buildShaderSourceFromFile( fEnvERName, 0 ) );
    ShowERProgram->link();

    if (ShowCubeProgram)
        delete ShowCubeProgram;

    ShowCubeProgram = new QOpenGLShaderProgram;
    ShowCubeProgram->addShaderFromSourceCode(
            QOpenGLShader::Vertex,
            buildShaderSourceFromFile( vEnvName, 0 ));
    ShowCubeProgram->addShaderFromSourceCode(
            QOpenGLShader::Fragment,
            buildShaderSourceFromFile( fEnvCubeName, 0 ) );
    ShowCubeProgram->link();

    if (ComputeCubeProgram)
        delete ComputeCubeProgram;

    ComputeCubeProgram = new QOpenGLShaderProgram;
    ComputeCubeProgram->addShaderFromSourceCode(
            QOpenGLShader::Compute,
            buildShaderSourceFromFile( ComputeName, 0 ) );
    ComputeCubeProgram->link();
}

void ViewData::setHFOV( const float p_hFOV )
{
    g_FOV.setX( p_hFOV );
    rebuildFOV();
}

void ViewData::rebuildFOV()
{
    const float width  = float( g_width  );
    const float height = float( g_height );

    const float scale = width / tanf( g_FOV.x() * 3.1415926f / 360.0f ); // Remember half-angle
    g_FOV.setY( atan2f( height, scale )         / 3.1415926f * 360.0f ); // Remember half-angle
}

void ViewData::drawCube( QOpenGLTexture* src )
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();

    if ( ShowCubeProgram->isLinked() )
    {
        glLock.lock();
        QMatrix3x3 M = ViewData::g_trackBall.rotation().toRotationMatrix();
        src->bind( 0 );
        //m_input.getTexture()->bind( 0 );
        ShowCubeProgram->bind();
        ShowCubeProgram->setUniformValue( "p_fov",  ViewData::g_FOV );
        ShowCubeProgram->setUniformValue( "p_view", M    );
        ShowCubeProgram->setUniformValue( "p_lens", ViewData::g_Lens);
        ShowCubeProgram->setUniformValue( "env",    0    );
        background.__draw__( f );
        ShowCubeProgram->release();
        //m_input.getTexture()->release();
        src->release();
        glLock.unlock();
    }

}

void ViewData::drawER(   QOpenGLTexture* src )
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();

    if ( ShowERProgram->isLinked() )
    {
        glLock.lock();
        QMatrix3x3 M = ViewData::g_trackBall.rotation().toRotationMatrix();
        src->bind( 0 );
        //m_input.getTexture()->bind( 0 );
        ShowERProgram->bind();
        ShowERProgram->setUniformValue( "p_fov",  ViewData::g_FOV );
        ShowERProgram->setUniformValue( "p_view", M    );
        ShowERProgram->setUniformValue( "p_lens", ViewData::g_Lens);
        ShowERProgram->setUniformValue( "env",    0    );
        background.__draw__( f );
        ShowERProgram->release();
        //m_input.getTexture()->release();
        src->release();
        glLock.unlock();
    }

}

void ViewData::computeCube( QOpenGLTexture* src, QOpenGLTexture* tgt )
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();

    //f->glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    //f->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    if ( ComputeCubeProgram->isLinked() )
    {
        f->glBindImageTexture(0, src->textureId(), 0, GL_FALSE, 0,  GL_READ_WRITE, GL_RGBA8);
        f->glBindImageTexture(1, tgt->textureId(), 0, GL_TRUE,  0,  GL_READ_WRITE, GL_RGBA8);

        ComputeCubeProgram->bind();
        int jobCount = mainTextureSize / computeGoupeSize;
        f->glDispatchCompute( jobCount, jobCount, 6 );
        f->glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
        ComputeCubeProgram->release();

        f->glBindImageTexture(0, 0, 0, 0, 0,  GL_READ_WRITE, GL_RGBA8);
        f->glBindImageTexture(1, 0, 0, 0, 0,  GL_READ_WRITE, GL_RGBA8);
    }

}

QVector2D ViewData::g_FOV; // degrees
QVector3D ViewData::g_Lens;
float     ViewData::g_FPS;
float     ViewData::g_FrameTime;
int       ViewData::g_width;
int       ViewData::g_height;

TrackBall           ViewData::g_trackBall;
QFileSystemWatcher* ViewData::g_watcher;

int       ViewData::mainTextureSize;
QString   ViewData::profile;

QMutex                           ViewData::glLock;
QVector< QOpenGLTexture* >       ViewData::g_textures;
Input                            ViewData::g_input;
QOpenGLShaderProgram*            ViewData::ShowERProgram;
QOpenGLShaderProgram*            ViewData::ShowCubeProgram;
QOpenGLShaderProgram*            ViewData::ComputeCubeProgram;

Trivial< 4, GL_TRIANGLE_STRIP >  ViewData::background;

