QT += opengl widgets
#requires(qtConfig(combobox))

#qtConfig(opengles.|angle|dynamicgl): error("This example requires Qt to be configured with -opengl desktop")

HEADERS += \
           primitives.h \
           gpuImpl.h \
           Viewport.h \
           trackball.h \
           MainWinPC.h \
           UI_pc/Tools.h \
           UI_pc/PropTree.h \
           microNN/Tensor.h \
           microNN/TensorOp.h \
           microNN/Layer.h \
           Input.h

SOURCES += \
           main.cpp \
           primitives.cpp \
           gpuImpl.cpp \
           Viewport.cpp \
           trackball.cpp \
           MainWinPC.cpp \
           UI_pc/Tools.cpp \
           UI_pc/PropTree.cpp \
           microNN/Tensor.cpp \
           microNN/TensorOp.cpp \
           microNN/Tensor_cpu.cpp \
           microNN/Tensor_gpu.cpp \
           microNN/gpu.cpp \
           microNN/Layer.cpp \
           Input.cpp

RESOURCES += Tool360.qrc

# install
#target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/boxes
#INSTALLS += target
TARGET = Tool360

FORMS += \
    UI_pc/ParamWidget.ui
