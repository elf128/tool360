/*
 * MainWinPC.cpp
 *
 *  Created on: Feb 28, 2020
 *      Author: vlad
 */

// This whole file is PC-only. Nothing should be build for Android.
#if !defined(__ANDROID_API__)
#include "MainWinPC.h"
#include "Viewport.h"
#include "gpuImpl.h"

#include "UI_pc/Tools.h"
#include "ui_ParamWidget.h"

#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>

MainWinPC::MainWinPC( QWidget *parent, Qt::WindowFlags flags )
    : QMainWindow(parent, flags)
{
    setObjectName("MainWinPC");
    setWindowTitle("Tool360");

    viewport = new Viewport( this );

    //const CustomSizeHintMap &customSizeHints,
    //QTextEdit *center = new QTextEdit(this);
    //center->setReadOnly(true);
    viewport->setMinimumSize(400, 200);
    setCentralWidget(viewport);

    setupProperties();

    makeToolBar();
    //setupMenuBar();
    makeDockWidgets();
    
    timer = new QTimer( this );
    timer->setInterval( 40 );
    connect( timer, SIGNAL( timeout() ), this, SLOT( update() ) );
    timer->start();

    //statusBar()->showMessage(tr("Status Bar"));
}

void MainWinPC::setupProperties()
{
    // Make sure you call it after viewport is valid and only once

    VoidProperty* tmpProp;
    properties.append( tmpProp = new VoidProperty( "FOV" ) );
    properties.append( new PropertyF( "H", ViewData::getHFOV, ViewData::setHFOV, tmpProp ) );
    properties.append( new PropertyF( "V", ViewData::getVFOV, nullptr,           tmpProp ) );

    properties.append( tmpProp = new VoidProperty( "Lens") );
    properties.append( new PropertyF( "d2",        nullptr, nullptr, tmpProp ) );
    properties.append( new PropertyF( "d4",        nullptr, nullptr, tmpProp ) );
    properties.append( new PropertyF( "st-offset", nullptr, nullptr, tmpProp ) );

}
void MainWinPC::makeToolBar()
{
    toolbar = new Toolbar( QString::fromLatin1("Tool Bar"), this );
    addToolBar( toolbar );
}

void MainWinPC::makeDockWidgets()
{
    params = new ViewParams( this, 0 );
    addDockWidget( Qt::RightDockWidgetArea, params );

    params->setupProps( &properties );

}

void MainWinPC::update()
{
    params->setStatus( QString("%1").arg( ViewData::getFPS(), 5, 'f', 1 ));
    params->getPropTree()->updateItems();
}

#endif

