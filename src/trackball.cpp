/****************************************************************************
**
** Copyright (c) 2020 Vlad A. <elf128@gmail.com>
**
** This file is based on example from the demonstration applications
** of the Qt Toolkit. Which are copyrighted by The Qt Company Ltd. 2016
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "trackball.h"
#include <qmath.h>
#include <math.h>
//#include "Viewport.h"

//============================================================================//
//                                  TrackBall                                 //
//============================================================================//

TrackBall::TrackBall(TrackMode p_mode)
    : m_paused          ( false )
    , m_pressed         ( false )
    , m_mode            ( p_mode )
{
    m_orientation = QQuaternion();
    m_velocity    = 0;
    m_axis        = QVector3D( 0.0f,0.0f,0.0f );
    m_lastTime    = QTime::currentTime();
}

TrackBall::TrackBall(
        const float      p_velocity,
        const QVector3D& p_axis,
        const TrackMode  p_mode)
    : m_orientation     ()
    , m_axis            ( p_axis          )
    , m_velocity        ( p_velocity / 360.0f * 3.1415926f )
    , m_paused          ( false           )
    , m_pressed         ( false           )
    , m_mode            ( p_mode          )
{
    m_lastTime    = QTime::currentTime();
}

void TrackBall::push( const QVector2D& p )
{
    m_orientation = m_lastOrientation = m_pushOrientation = rotation();
    m_pressed     = true;
    m_lastTime    = QTime::currentTime();

    m_lastPos     = QVector3D( p.x(), 0.0f, p.y() );

    if ( m_lastPos.lengthSquared() > 1.0f )
        m_lastPos.normalize();
    else
        m_lastPos.setY( sqrtf( 1.0f - p.lengthSquared() ) );

    m_velocity    = 0.0f;
    m_axis        = QVector3D();
}

void TrackBall::move( const QVector2D& p )
{
    if (!m_pressed)
        return;

    m_lastTime = QTime::currentTime();
    m_lastOrientation = m_orientation;

    switch (m_mode)
    {
        case TrackMode::Plane:
            {
/*                QLineF delta(m_lastPos, p);
                const float angleDelta = qRadiansToDegrees(float(delta.length()));
                m_angularVelocity = angleDelta / msecs;
                m_axis = QVector3D( -delta.dy(), delta.dx(), 0.0f ).normalized();
                m_axis = transformation.rotatedVector( m_axis );
                m_orientation = QQuaternion::fromAxisAndAngle( m_axis, angleDelta ) * m_orientation;*/
            }
            break;
        case TrackMode::Sphere:
            {
                QVector3D currentPos3D = QVector3D( p.x(), 0.0f, p.y() );
                if ( currentPos3D.lengthSquared() > 1.0f )
                    currentPos3D.normalize();
                else
                    currentPos3D.setY( sqrtf( 1.0f - p.lengthSquared() ) );

                QQuaternion rot = QQuaternion::rotationTo( m_lastPos, currentPos3D );
                //m_axis = QVector3D::crossProduct(lastPos3D, currentPos3D);
                //float angle = asinf(m_axis.length());

                //m_velocity = angle / msecs * 0.5f;
                //m_axis.normalize();
                //m_axis = transformation.rotatedVector(m_axis);
                m_orientation = rot * m_pushOrientation;
            }
            break;
    }

}

void TrackBall::release()
{
    if (!m_pressed)
        return;

    m_pressed = false;

    QTime currentTime = QTime::currentTime();
    int msecs = m_lastTime.msecsTo(currentTime);

    msecs = ( msecs > 10 ) ? msecs : 10;

    QQuaternion dO = ( m_orientation * m_lastOrientation.inverted());

    if ( dO.scalar() >= 1.0f )
        return;

    m_velocity = acosf( dO.scalar() ) / msecs;
    if ( m_velocity > 0.0001f )
        m_axis     = dO.vector().normalized();
    else
        m_velocity = 0.0f;
}

void TrackBall::start()
{
    m_lastTime = QTime::currentTime();
    m_paused = false;
}

void TrackBall::stop()
{
    m_orientation = rotation();
    m_paused = true;
}

QQuaternion TrackBall::rotation() const
{
    if (m_paused || m_pressed)
        return m_orientation;

    QTime currentTime = QTime::currentTime();

    const float dA = m_velocity * m_lastTime.msecsTo( currentTime );

    const float sA = sinf( dA );
    const float cA = cosf( dA );

    return QQuaternion( cA, m_axis * sA ) * m_orientation;
}

