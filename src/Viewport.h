
#ifndef VIEWPORT_H
#define VIEWPORT_H

#if defined(__ANDROID_API__)
#include <QOpenGLWindow>
#define CLASS_PARENT QOpenGLWindow
#define CLASS_PARAM QWindow
#else
#include <QOpenGLWidget>
#define CLASS_PARENT QOpenGLWidget
#define CLASS_PARAM QWidget
#endif

#include <QTime>
#include <QTimer>
#include <QFileSystemWatcher>

class Viewport : public CLASS_PARENT
{
    Q_OBJECT

public:
    Viewport(  CLASS_PARAM *parent = Q_NULLPTR );
    ~Viewport() override;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

public slots:
    void reloadProxy();
protected:
#if defined(__ANDROID_API__)
    void touchEvent(       QTouchEvent* event) override;
#else
    void mousePressEvent(  QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(   QMouseEvent* event) override;
    void wheelEvent(       QWheelEvent* event) override;
#endif

private:
    int   m_frame;
    int   m_ms;
    int   m_lastTime;

    QTime     m_time;
    QTimer*   m_timer;
    QFileSystemWatcher m_watcher;
};

#endif
