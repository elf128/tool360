#include "PropTree.h"

PropTree::PropTree( QWidget* parent )
    : QTreeWidget( parent )
    , extProps( nullptr )
{
    setColumnCount(2);
    connect(this, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(Clicked(QTreeWidgetItem*, int)));
}

void PropTree::fillItems( QList< VoidProperty* >* p_Props )
{
    QList<QTreeWidgetItem *> items;
    extProps = p_Props;

    foreach( auto prop, *extProps )
    {
        if ( prop->item == nullptr )
            prop->item = new QTreeWidgetItem( (QTreeWidget*)0, QStringList( prop->name ) );

        if ( prop->parent && prop->parent->item )
            prop->parent->item->addChild( prop->item );
        else
            items.append( prop->item );
    }

    addTopLevelItems( items );

}

void PropTree::updateItems()
{
    foreach( auto prop, *extProps )
    {
        if ( prop->item != nullptr )
        {
            if ( prop->getType() == PropType::Float && static_cast<PropertyF*>(prop)->getter != nullptr )
                prop->item->setText( 1, QString("%1").arg( static_cast<PropertyF*>(prop)->getter() ) );
        }

    }
}

void PropTree::Clicked(QTreeWidgetItem *item, int column)
{
    (void)column ;
    if ( item->childCount() )
        item->setExpanded( !item->isExpanded() );
}
