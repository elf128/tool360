#ifndef PROPTREE_H
#define PROPTREE_H

#include <QWidget>
#include <QTreeWidget>

enum class PropType
{
    None,
    Float,
    Float2,
    Float3,
    Float4,
    Int
};

typedef float     (*Getter)();
typedef QVector2D (*Getter2)();
typedef QVector3D (*Getter3)();
typedef QVector4D (*Getter4)();
typedef int       (*GetterI)();

typedef void (*Setter)( const float);
typedef void (*Setter2)(const QVector2D);
typedef void (*Setter3)(const QVector3D);
typedef void (*Setter4)(const QVector4D);
typedef void (*SetterI)(const int );

struct VoidProperty
{
    QString          name;
    QTreeWidgetItem* item;
    VoidProperty*    parent;
    VoidProperty( QString p_name, VoidProperty* p_parent = nullptr, PropType p_type = PropType::None )
        : name  ( p_name )
        , item  ( nullptr )
        , parent( p_parent )
        , type  ( p_type )
    {}

    inline PropType getType() const { return type; }
private:
    PropType         type;
};

template < PropType propType, typename toGet, typename toSet >
struct Property : public VoidProperty
{
    toGet         getter;
    toSet         setter;

    Property( QString p_name, toGet p_getter, toSet p_setter, VoidProperty* p_parent = nullptr )
        : VoidProperty( p_name, p_parent, propType )
        , getter( p_getter )
        , setter( p_setter )
    {}
};

using PropertyF  = Property<PropType::Float,  Getter, Setter  >;
using PropertyF2 = Property<PropType::Float2, Getter2,Setter2 >;
using PropertyF3 = Property<PropType::Float3, Getter3,Setter3 >;
using PropertyF4 = Property<PropType::Float4, Getter4,Setter4 >;

class PropTree : public QTreeWidget
{
    Q_OBJECT
public:
    PropTree( QWidget* parent );
    void fillItems( QList< VoidProperty* >* p_Props );

public slots:
    void Clicked(QTreeWidgetItem *item, int column);
    void updateItems();

protected:

    QList< VoidProperty* >* extProps;
};

#endif // PROPTREE_H
