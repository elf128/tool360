/*
 * Tools.cpp
 *
 *  Created on: Feb 28, 2020
 *      Author: vlad
 */

#include "Tools.h"
#include "ui_ParamWidget.h"

#include <QMainWindow>
#include <QMenu>


Toolbar::Toolbar(const QString &title, QWidget *parent, const int iconSize )
    : QToolBar(parent)
{
    setWindowTitle(title);
    setObjectName(title);

    QImage img( ":/res/qt-logo.png");
    QPixmap pix = QPixmap::fromImage(img.scaled( iconSize, iconSize ), Qt::DiffuseDither | Qt::DiffuseAlphaDither);

    setIconSize( QSize( iconSize, iconSize ) );

    addAction( pix, "QQQQ");

}

ViewParams::ViewParams( QMainWindow *parent, Qt::WindowFlags flags )
    : QDockWidget(parent, flags)
    , mainWindow(parent)
    , ui( new Ui::properties )
{
    ui->setupUi( this );
}

void ViewParams::setupProps( QList< VoidProperty* >* p_Props )
{
    ui->tree->fillItems( p_Props );
}

void ViewParams::setStatus( QString status )
{
    ui->status->setText( status );
}
