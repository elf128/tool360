/*
 * Tools.h
 *
 *  Created on: Feb 28, 2020
 *      Author: Vlad A. <elf128@gmail.com>
 */

#ifndef UI_PC_TOOLS_H_
#define UI_PC_TOOLS_H_


#include <QToolBar>
#include <QDockWidget>
#include "ui_ParamWidget.h"
#include "PropTree.h"


QT_FORWARD_DECLARE_CLASS(QAction)
QT_FORWARD_DECLARE_CLASS(QActionGroup)
QT_FORWARD_DECLARE_CLASS(QMenu)

class Toolbar : public QToolBar
{
    Q_OBJECT
public:
    explicit Toolbar( const QString &title, QWidget *parent, const int iconSize = 24 );

};

class ViewParams : public QDockWidget
{
    Q_OBJECT
public:
    explicit ViewParams( QMainWindow *parent = Q_NULLPTR, Qt::WindowFlags flags = 0 );

    void setStatus( QString status );
    void setupProps( QList< VoidProperty* >* p_Props );

    inline PropTree* getPropTree() { return ui->tree; }
private:
    QMainWindow *mainWindow;
    Ui::properties *ui;
};

#endif /* UI_PC_TOOLS_H_ */
