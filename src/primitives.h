
#ifndef PRIMITIVES_H
#define PRIMITIVES_H

//#include "glextensions.h"

#include <QtWidgets>
#include <QtOpenGL>

#include <QtGui/qvector3d.h>
#include <QtGui/qvector2d.h>
#include "stdint.h"


template < uint16_t vtxCount, uint16_t mode >
class Trivial
{
public:
    Trivial() : vao( nullptr ) {}
    virtual ~Trivial() {}

    virtual void __init__()
    {
        if ( vao ) delete vao;
        vao = new QOpenGLVertexArrayObject;
        vao->create();
    }

    virtual void __destroy__() {if ( vao ) delete vao; }
    virtual void __draw__( QOpenGLExtraFunctions* extra )
    {
        vao->bind();
        extra->glDrawArrays( mode, 0, vtxCount );
        vao->release();
    }

private:
    QOpenGLVertexArrayObject *vao;
};

template < uint16_t vtxCount, uint16_t mode >
class Primitive
{
public:
    Primitive() {}
    virtual ~Primitive() {}

};

class Quad : public Primitive< 4, GL_TRIANGLE_STRIP >
{
public:
    Quad() {}
    virtual ~Quad() {}
};

#endif
